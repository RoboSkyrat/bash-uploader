using Microsoft.EntityFrameworkCore;

namespace Uploader.Models
{
    public class FileContext : DbContext
    {
        public FileContext(DbContextOptions<FileContext> options)
            : base(options)
        {
        }

        public DbSet<FileRecord> FileRecords { get; set; }
    }
}