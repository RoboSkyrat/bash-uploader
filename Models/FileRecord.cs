using System.ComponentModel.DataAnnotations;

namespace Uploader.Models
{
    public class FileRecord
    {
        [Key]
        public string Filename { get; set; }
        public string Secret { get; set; }
    }
}