# Bash Uploader
This is an uploader created by IKnowBashFu to be an easy to use personal image uploader

## Getting Started
This project is mainly used inside of docker so the following instructions will be for docker.

### Deployment
To deploy this to a production machine you must install docker, These steps assume you have docker installed already.

 First, Clone the project and cd into the directory
```
git clone https://bitbucket.org/iknowbashfu/bash-uploader.git bash-uploader
cd bash-uploader
```

Next, You need to tell docker to build the container
```
docker build . -t bash-uploader
```

You should follow the [Configuration](#configuration) section before continuing with deployment.

Finally, You just need to run
```
docker run -v /path/to/appsettings.json:/app/appsettings.json -v /path/to/database.sqlite:/app/database.sqlite -v /path/to/wwwroot:/app/wwwroot -p 80:80 bash-uploader
```

### Configuration

You need to copy `appsettings.example.json` and rename it to `appsettings.json`,
After doing that you need to edit the file and change

```
  "Settings": {
    "UploadSecret": "changeme",
    "FilenameLength": 15,
    "SecretLength": 3
  }
```

`UploadSecret` is the password used to allow uploading.

`FilenameLength` is the length of the random string used for filename. The default is 15.

`SecretLength` is the length if the deletion secret. (Used to delete files after they are uploaded.)