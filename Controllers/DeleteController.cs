using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Uploader.Models;

namespace Uploader.Controllers
{
    [Route("api/[controller]")]
    public class DeleteController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly FileContext _fileContext;

        public DeleteController(IHostingEnvironment hostingEnvironment, FileContext fileContext)
        {
            _hostingEnvironment = hostingEnvironment;
            _fileContext = fileContext;
        }

        [HttpGet("{filename}")]
        public IActionResult Delete(string filename)
        {
            string webroot = _hostingEnvironment.WebRootPath;
            string filePath = Path.Combine(webroot, filename);

            string secret = Request.Query.SingleOrDefault(qp => qp.Key.ToLower() == "secret").Value;

            if (System.String.IsNullOrEmpty(secret))
            {
                return BadRequest();
            }

            FileRecord fr = _fileContext.FileRecords.Find(filename);
            if (fr == null)
            {
                return NotFound();
            }
            
            if (fr.Secret == secret)
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }

                _fileContext.FileRecords.Remove(fr);
                _fileContext.SaveChanges();

                return NoContent();
            }

            return Unauthorized();
        }
    }
}