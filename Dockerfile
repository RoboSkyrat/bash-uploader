FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

#copy csproj and restore
COPY *.csproj ./
RUN dotnet restore

#copy everything and build
COPY . ./
RUN dotnet publish -c release -o out

#build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Uploader.dll"]